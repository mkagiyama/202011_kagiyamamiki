$(document).ready(function(){
	 $("#form").validationEngine('attach', {
		validationEventTrigger:'keyup',
		promptPosition: "centerRight",
		autoHidePrompt: true,
		autoHideDelay: 5000
	 });
  });
