package jp.alhinc.springtraining.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;

	public int getLoginId(String loginId) {
		return mapper.getLoginId(loginId);
	}

	@Transactional
	public int create(CreateUserForm form) {
		User entity = new User();
		BeanUtils.copyProperties(form, entity); //FormからEntityへ詰め替え

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));

		int count = mapper.create(entity);
		return count;
	}
}
