package jp.alhinc.springtraining.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.SettingsUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class SettingService {

	@Autowired
	private UserMapper mapper;

	public int getLoginId(String loginId) {
		return mapper.getLoginId(loginId);
	}

	public SettingsUserForm getSettingUser(Long id){
		User settingUser = mapper.getUser(id); //画面から送られたidを元にユーザ情報を取得
		SettingsUserForm form = new SettingsUserForm();
		BeanUtils.copyProperties(settingUser, form); //EntityからFormへ詰め替え
		form.setRawPassword(settingUser.getPassword());
		return form;
	}

	public int updateUser(SettingsUserForm form) {
		User entity = new User();
		BeanUtils.copyProperties(form, entity);

		//パスワードが空白ではない場合はパスワードを暗号化
		if(!form.getRawPassword().isEmpty()) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			entity.setPassword(encoder.encode(form.getRawPassword()));
		}
		return mapper.updateUser(entity);
	}

	public int deleteUser(Long id) {
		return mapper.deleteUser(id);
	}

	public int restoreUser(Long id) {
		return mapper.restoreUser(id);
	}
}
