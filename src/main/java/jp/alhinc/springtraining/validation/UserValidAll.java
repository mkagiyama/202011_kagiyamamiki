package jp.alhinc.springtraining.validation;

import javax.validation.GroupSequence;

@GroupSequence({UserValidGroup1.class, UserValidGroup2.class, UserValidGroup3.class})
public interface UserValidAll {

}
