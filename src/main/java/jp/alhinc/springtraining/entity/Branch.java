package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class Branch {
	public int id;
	public String branch_name;
}
