package jp.alhinc.springtraining.entity;

import java.util.Date;

import lombok.Data;

@Data
public class User {

	private Long id;
	public String loginId;
	public String password;
	public String name;
	public int branch_id;
	public String branch_name;
	public int department_position;
	public String department_position_name;
	public boolean is_delete;
    public Date created_at;
    public Date updated_at;

}
