package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class DepartmentPosition {
	public int id;
	public String department_position_name;
}
