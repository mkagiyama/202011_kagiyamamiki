package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.alhinc.springtraining.entity.User;

@Mapper
public interface UserMapper {

	List<User> findAll();

	int getLoginId(@Param(value="loginId")String loginId);

	int create(User entity);

	User getUser(@Param(value="id")Long id);

	int updateUser(User entity);

	int deleteUser(@Param(value="id")Long id);

	int restoreUser(@Param(value="id")Long id);

}
