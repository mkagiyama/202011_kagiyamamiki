package jp.alhinc.springtraining.form;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.AssertTrue;

import lombok.Data;

@Data
public class SettingsUserForm implements Serializable {
	public Long id;

	public String loginId;
	//loginIdが空白の場合はtrueとして処理
	@AssertTrue(message="半角英数字で6文字から20文字で入力して下さい")
	public boolean isLoginIdValid() {
		if(loginId == null || loginId.isEmpty() || (loginId.matches("[a-zA-Z0-9]+$") &&
				loginId.length() >= 6 && loginId.length() <= 20)) return true;
		return false;
	}

	public String rawPassword;
	@AssertTrue(message="記号を含む全ての半角英数字で6文字から20文字で入力して下さい")
	//rawPsswordとsecondPasswordが共に空白の場合はtrueとして処理
	public boolean isRawPasswordValid() {
		if((rawPassword == null || rawPassword.isEmpty()) && (secondPassword == null || secondPassword.isEmpty()) ||
				(rawPassword.matches("[\\p{Alnum}|\\p{Punct}]*$") && rawPassword.length() >= 6 && rawPassword.length() <= 20))
			return true;
		return false;
	}

	public String secondPassword;

	//rawPasswordが入力されていてsecondPasswordが空白の場合はエラー表示
	@AssertTrue(message="エラーです")
	public boolean isPasswordValid() {
		if(rawPassword == null || rawPassword.isEmpty()) return true;
		return rawPassword.equals(secondPassword);
	}

	public String name;

	@AssertTrue(message="10文字以下の全角・半角英文字で入力して下さい")
	//nameが空白の場合はtrueとして処理
	public boolean isNameValid() {
		if(name == null || name.isEmpty() || (name.matches("[a-zA-Zぁ-ゞァ-ヶ一-龠・ー]+$") && name.length() <= 10))
			return true;
		return false;
	}

	public int branch_id;
	public int department_position;
	public boolean is_delete;
	public Date created_at;
	public Date updated_at;

}
