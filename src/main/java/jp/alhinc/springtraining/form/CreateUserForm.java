package jp.alhinc.springtraining.form;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class CreateUserForm implements Serializable {

//	@NotBlank(message="必須項目です", groups = UserValidGroup1.class)
//	@Size(min=6, max=20,message="6文字から20文字で入力して下さい", groups = UserValidGroup2.class)
//	@Pattern(regexp = "^[A-Za-z0-9]+$", message="半角英数字のみ有効です", groups = UserValidGroup3.class)
	public String loginId;

//	@NotBlank(message="必須項目です", groups = UserValidGroup1.class)
//	@Size(min=6, max=20,message="6文字から20文字で入力して下さい", groups = UserValidGroup2.class)
//	@Pattern(regexp = "^[\\p{Alnum}|\\p{Punct}]*$", message="記号を含む全ての半角文字のみ有効です", groups = UserValidGroup3.class)
	public String rawPassword;

//	@NotBlank(message="確認用パスワードを入力してください", groups = UserValidGroup1.class)
	public String secondPassword;

	//rawPasswordが入力されていてsecondPasswordが空白の場合はエラー表示
//	@AssertTrue(message="エラーです", groups = UserValidGroup1.class)
	public boolean isPasswordValid() {
		if(rawPassword == null || rawPassword.isEmpty()) return true;
		return rawPassword.equals(secondPassword);
	}

//	@NotBlank(message="必須項目です", groups = UserValidGroup1.class)
//	@Size(max=10,message="10文字以下で入力して下さい", groups = UserValidGroup2.class)
//	@Pattern(regexp = "^[a-zA-Zぁ-ゞァ-ヶ一-龠・ー]+$", message="全角文字、半角英文字、『・』、『－』が有効です", groups = UserValidGroup3.class)
	public String name;

	public int branch_id;
	public int department_position;
	public boolean is_delete;
	public Date created_at;
	public Date updated_at;
}
