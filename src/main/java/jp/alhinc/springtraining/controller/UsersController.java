package jp.alhinc.springtraining.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.DepartmentPosition;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.SettingsUserForm;
import jp.alhinc.springtraining.service.BranchService;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.DepartmentPositionService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.SettingService;
import jp.alhinc.springtraining.validation.UserValidAll;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentPositionService departmentPositionService;

	@Autowired
	private SettingService settingService;

	public static final String STR_BRANCHES = "branches";
	public static final String STR_DEPARTMENTS_POSITIONS = "departmentsPositions";
	public static final String MESSAGES = "messages";
	public static final String FORM = "form";
	public static final String USERS = "users";

	@GetMapping
	public String index(Model model) {
		final List<User> users = getAllUsersService.getAllUsers();
		model.addAttribute(USERS, users);
		return "users/index";
	}

	@GetMapping("/create")
	public String create(Model model) {
		final List<Branch> branches = branchService.getBranch(); //支店情報取得
		final List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition(); //部署・役職情報取得
		model.addAttribute(FORM, new CreateUserForm());
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENTS_POSITIONS, departmentsPositions);
		return "users/create";
	}

	@GetMapping("/settings")
	public String settings(@RequestParam Long id, Model model) {
		final SettingsUserForm form = settingService.getSettingUser(id);
		final List<Branch> branches = branchService.getBranch(); //支店情報取得
		final List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition(); //部署・役職情報取得
		model.addAttribute(FORM, form);
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENTS_POSITIONS, departmentsPositions);
		return "users/settings";
	}

	@PostMapping
	public String create(@ModelAttribute(FORM) @Validated(UserValidAll.class) CreateUserForm form, BindingResult result, Model model) {
		final List<Branch> branches = branchService.getBranch(); //支店情報取得
		final List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition(); //部署・役職情報取得

		//入力チェックでエラー
		if (result.hasErrors()) {
			model.addAttribute(MESSAGES, "以下の内容を確認してください");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENTS_POSITIONS, departmentsPositions);
			return "users/create";
		}

		//DBに同じログインIDがないかチェック
		int count = createUserService.getLoginId(form.getLoginId());
		if(count == 0) {
			createUserService.create(form);
			return "redirect:/users";

		//1以上の場合はエラーメッセージを表示
		} else {
			model.addAttribute(MESSAGES, "重複したログインIDが存在しています。");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENTS_POSITIONS, departmentsPositions);
			return "users/create";
		}
	}

	@PostMapping("/updated")
	public String updateUser(@ModelAttribute(FORM) @Valid SettingsUserForm form, BindingResult result, Model model) {
		final List<Branch> branches = branchService.getBranch(); //支店情報取得
		final List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition(); //部署・役職情報取得

		//入力チェックでエラー
		if(result.hasErrors()) {
			model.addAttribute(MESSAGES, "以下の内容を確認してください");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENTS_POSITIONS, departmentsPositions);
			return "users/settings";
		}
		SettingsUserForm user = settingService.getSettingUser(form.id); //表示ユーザの情報を取得

		//入力されたログインIDとDBに登録されているログインIDが同じ場合は更新する
		if(form.getLoginId().equals(user.getLoginId())) {
			settingService.updateUser(form);
			return "redirect:/users";
		} else {

			//DBに同じログインIDがないかチェック
			int count = settingService.getLoginId(form.getLoginId());
			if(count == 0) {
				settingService.updateUser(form);
				return "redirect:/users";

			//1以上の場合はエラーメッセージを表示
			} else {
				model.addAttribute(MESSAGES, "重複したログインIDが存在しています。");
				model.addAttribute(STR_BRANCHES, branches);
				model.addAttribute(STR_DEPARTMENTS_POSITIONS, departmentsPositions);
				return "users/settings";
			}
		}
	}

	@PostMapping("/delete")
	public String deleteUser(@ModelAttribute(USERS) User users, Model model) {
		Long id = users.getId();
		settingService.deleteUser(id);
		return "redirect:/users";
	}

	@PostMapping("/restore")
	public String restoreUser(@ModelAttribute(USERS) User users, Model model) {
		Long id = users.getId();
		settingService.restoreUser(id);
		return "redirect:/users";
	}
}
